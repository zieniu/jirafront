'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">jira-front documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AngularMaterialModule.html" data-type="entity-link">AngularMaterialModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-c5403bc172f7a965bbaa972a805248bc"' : 'data-target="#xs-components-links-module-AppModule-c5403bc172f7a965bbaa972a805248bc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-c5403bc172f7a965bbaa972a805248bc"' :
                                            'id="xs-components-links-module-AppModule-c5403bc172f7a965bbaa972a805248bc"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConfirmDialogModule.html" data-type="entity-link">ConfirmDialogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConfirmDialogModule-b87ddb39dd2d26f97f664b2aaf21b71e"' : 'data-target="#xs-components-links-module-ConfirmDialogModule-b87ddb39dd2d26f97f664b2aaf21b71e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfirmDialogModule-b87ddb39dd2d26f97f664b2aaf21b71e"' :
                                            'id="xs-components-links-module-ConfirmDialogModule-b87ddb39dd2d26f97f664b2aaf21b71e"' }>
                                            <li class="link">
                                                <a href="components/ConfirmDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DragAndDropModule.html" data-type="entity-link">DragAndDropModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DragAndDropModule-5bd48e5d843fd740677d9988226dc495"' : 'data-target="#xs-components-links-module-DragAndDropModule-5bd48e5d843fd740677d9988226dc495"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DragAndDropModule-5bd48e5d843fd740677d9988226dc495"' :
                                            'id="xs-components-links-module-DragAndDropModule-5bd48e5d843fd740677d9988226dc495"' }>
                                            <li class="link">
                                                <a href="components/DragAndDropComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DragAndDropComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link">HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-54e6109ac333446324c1c936c625d2a6"' : 'data-target="#xs-components-links-module-HomeModule-54e6109ac333446324c1c936c625d2a6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-54e6109ac333446324c1c936c625d2a6"' :
                                            'id="xs-components-links-module-HomeModule-54e6109ac333446324c1c936c625d2a6"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeRoutingModule.html" data-type="entity-link">HomeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link">LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-2280bc398b19f01b33646745c7f0e833"' : 'data-target="#xs-components-links-module-LoginModule-2280bc398b19f01b33646745c7f0e833"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-2280bc398b19f01b33646745c7f0e833"' :
                                            'id="xs-components-links-module-LoginModule-2280bc398b19f01b33646745c7f0e833"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link">LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NofiticationMoreInfoDetailRoutingModule.html" data-type="entity-link">NofiticationMoreInfoDetailRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationAssignDialogModule.html" data-type="entity-link">NotificationAssignDialogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationAssignDialogModule-ee164e54d7292e5331e1220d857ee1cb"' : 'data-target="#xs-components-links-module-NotificationAssignDialogModule-ee164e54d7292e5331e1220d857ee1cb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationAssignDialogModule-ee164e54d7292e5331e1220d857ee1cb"' :
                                            'id="xs-components-links-module-NotificationAssignDialogModule-ee164e54d7292e5331e1220d857ee1cb"' }>
                                            <li class="link">
                                                <a href="components/NotificationAssignDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationAssignDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationChangeStatusDialogModule.html" data-type="entity-link">NotificationChangeStatusDialogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationChangeStatusDialogModule-29347e808e2529979fe4c38870e60811"' : 'data-target="#xs-components-links-module-NotificationChangeStatusDialogModule-29347e808e2529979fe4c38870e60811"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationChangeStatusDialogModule-29347e808e2529979fe4c38870e60811"' :
                                            'id="xs-components-links-module-NotificationChangeStatusDialogModule-29347e808e2529979fe4c38870e60811"' }>
                                            <li class="link">
                                                <a href="components/NotificationChangeStatusDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationChangeStatusDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationCreateNewDialogModule.html" data-type="entity-link">NotificationCreateNewDialogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationCreateNewDialogModule-23b90668efeff7bedc665dfb51e2282e"' : 'data-target="#xs-components-links-module-NotificationCreateNewDialogModule-23b90668efeff7bedc665dfb51e2282e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationCreateNewDialogModule-23b90668efeff7bedc665dfb51e2282e"' :
                                            'id="xs-components-links-module-NotificationCreateNewDialogModule-23b90668efeff7bedc665dfb51e2282e"' }>
                                            <li class="link">
                                                <a href="components/NotificationCreateNewDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationCreateNewDialogComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationMoreInfoDetailModule.html" data-type="entity-link">NotificationMoreInfoDetailModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' : 'data-target="#xs-components-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' :
                                            'id="xs-components-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' }>
                                            <li class="link">
                                                <a href="components/NotificationMoreInfoDetailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationMoreInfoDetailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' : 'data-target="#xs-injectables-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' :
                                        'id="xs-injectables-links-module-NotificationMoreInfoDetailModule-fc0bc4ab05b9a5074aee50cf09572f1d"' }>
                                        <li class="link">
                                            <a href="injectables/NotificationStatusService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>NotificationStatusService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationMoreInfoModule.html" data-type="entity-link">NotificationMoreInfoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationMoreInfoModule-57370ce5a1092ef57f6e85e8545e93e1"' : 'data-target="#xs-components-links-module-NotificationMoreInfoModule-57370ce5a1092ef57f6e85e8545e93e1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationMoreInfoModule-57370ce5a1092ef57f6e85e8545e93e1"' :
                                            'id="xs-components-links-module-NotificationMoreInfoModule-57370ce5a1092ef57f6e85e8545e93e1"' }>
                                            <li class="link">
                                                <a href="components/NotificationMoreInfoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationMoreInfoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationsListByTypeModule.html" data-type="entity-link">NotificationsListByTypeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' : 'data-target="#xs-components-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' :
                                            'id="xs-components-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' }>
                                            <li class="link">
                                                <a href="components/NotificationsListByTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationsListByTypeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' : 'data-target="#xs-injectables-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' :
                                        'id="xs-injectables-links-module-NotificationsListByTypeModule-4194626371f82c25419130c6ddd33685"' }>
                                        <li class="link">
                                            <a href="injectables/NotificationStatusService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>NotificationStatusService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationsTabsModule.html" data-type="entity-link">NotificationsTabsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-NotificationsTabsModule-1e15810d2143c378ecd24b708ce4fb97"' : 'data-target="#xs-components-links-module-NotificationsTabsModule-1e15810d2143c378ecd24b708ce4fb97"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-NotificationsTabsModule-1e15810d2143c378ecd24b708ce4fb97"' :
                                            'id="xs-components-links-module-NotificationsTabsModule-1e15810d2143c378ecd24b708ce4fb97"' }>
                                            <li class="link">
                                                <a href="components/NotificationsTabsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationsTabsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/NotificationsTabsRoutingModule.html" data-type="entity-link">NotificationsTabsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PagesModule.html" data-type="entity-link">PagesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PagesModule-84b005e3757107ea10d010ae8db137d0"' : 'data-target="#xs-components-links-module-PagesModule-84b005e3757107ea10d010ae8db137d0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PagesModule-84b005e3757107ea10d010ae8db137d0"' :
                                            'id="xs-components-links-module-PagesModule-84b005e3757107ea10d010ae8db137d0"' }>
                                            <li class="link">
                                                <a href="components/PagesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PagesComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PagesRoutingModule.html" data-type="entity-link">PagesRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/RegisterModule.html" data-type="entity-link">RegisterModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RegisterModule-defc0f50698b24b693764afeafd4bb4d"' : 'data-target="#xs-components-links-module-RegisterModule-defc0f50698b24b693764afeafd4bb4d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RegisterModule-defc0f50698b24b693764afeafd4bb4d"' :
                                            'id="xs-components-links-module-RegisterModule-defc0f50698b24b693764afeafd4bb4d"' }>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegisterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RegisterRoutingModule.html" data-type="entity-link">RegisterRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-14af900e1164a6c90ccd8548e9d9161d"' : 'data-target="#xs-pipes-links-module-SharedModule-14af900e1164a6c90ccd8548e9d9161d"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-14af900e1164a6c90ccd8548e9d9161d"' :
                                            'id="xs-pipes-links-module-SharedModule-14af900e1164a6c90ccd8548e9d9161d"' }>
                                            <li class="link">
                                                <a href="pipes/EnumPermissionToStrPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EnumPermissionToStrPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/EnumToArrayPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EnumToArrayPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/StatusNotificationNumberToStrPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StatusNotificationNumberToStrPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/TypeNotificationPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TypeNotificationPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ToolbarModule.html" data-type="entity-link">ToolbarModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' : 'data-target="#xs-components-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' :
                                            'id="xs-components-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' }>
                                            <li class="link">
                                                <a href="components/ToolbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ToolbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' : 'data-target="#xs-injectables-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' :
                                        'id="xs-injectables-links-module-ToolbarModule-da9bbdf2224131c39259b9a0d2ce7ebb"' }>
                                        <li class="link">
                                            <a href="injectables/NotificationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>NotificationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserBottomListModule.html" data-type="entity-link">UserBottomListModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserBottomListModule-e97f4be291e55575c259fb2d566698c4"' : 'data-target="#xs-components-links-module-UserBottomListModule-e97f4be291e55575c259fb2d566698c4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserBottomListModule-e97f4be291e55575c259fb2d566698c4"' :
                                            'id="xs-components-links-module-UserBottomListModule-e97f4be291e55575c259fb2d566698c4"' }>
                                            <li class="link">
                                                <a href="components/UserBottomListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserBottomListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserProfileModule.html" data-type="entity-link">UserProfileModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserProfileModule-4a20fcee8b74683991c80ca31b1e18ac"' : 'data-target="#xs-components-links-module-UserProfileModule-4a20fcee8b74683991c80ca31b1e18ac"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserProfileModule-4a20fcee8b74683991c80ca31b1e18ac"' :
                                            'id="xs-components-links-module-UserProfileModule-4a20fcee8b74683991c80ca31b1e18ac"' }>
                                            <li class="link">
                                                <a href="components/UserProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserProfileComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserProfileRoutingModule.html" data-type="entity-link">UserProfileRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-522a5b32aa83c8436bc38040b2960c9d"' : 'data-target="#xs-components-links-module-UsersModule-522a5b32aa83c8436bc38040b2960c9d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-522a5b32aa83c8436bc38040b2960c9d"' :
                                            'id="xs-components-links-module-UsersModule-522a5b32aa83c8436bc38040b2960c9d"' }>
                                            <li class="link">
                                                <a href="components/UserChangePermissionDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserChangePermissionDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersRoutingModule.html" data-type="entity-link">UsersRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/NewNotification.html" data-type="entity-link">NewNotification</a>
                            </li>
                            <li class="link">
                                <a href="classes/Notification.html" data-type="entity-link">Notification</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateNotification.html" data-type="entity-link">UpdateNotification</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthenticationService.html" data-type="entity-link">AuthenticationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationHttpService.html" data-type="entity-link">NotificationHttpService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationService.html" data-type="entity-link">NotificationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationStatusService.html" data-type="entity-link">NotificationStatusService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RoleGuardService.html" data-type="entity-link">RoleGuardService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SnackBarService.html" data-type="entity-link">SnackBarService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StorageUserService.html" data-type="entity-link">StorageUserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserHttpService.html" data-type="entity-link">UserHttpService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/ErrorInterceptor.html" data-type="entity-link">ErrorInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/JwtInterceptor.html" data-type="entity-link">JwtInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/RoleGuard.html" data-type="entity-link">RoleGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/RegisterUser.html" data-type="entity-link">RegisterUser</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SelectItem.html" data-type="entity-link">SelectItem</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});