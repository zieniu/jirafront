import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import * as signalR from '@microsoft/signalr';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';

@Injectable({
  providedIn: 'root',
})
export class SignalRService {
  
  /**
   * Podłączenie do serwera
   * @param channel kanał signalr
   */
  connectToServer(channel: string,next:() => void): HubConnection {
    const connection = new HubConnectionBuilder().withUrl(`http://localhost:4555/${channel}`)
    .configureLogging(signalR.LogLevel.Information)
    .withAutomaticReconnect()
    .build();

    connection
      .start()
      .then(next)
      .catch(() => console.log(`Error while establishing connection :( - ${channel}`));

    return connection;
  }
}
