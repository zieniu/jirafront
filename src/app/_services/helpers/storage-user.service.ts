import { Injectable } from '@angular/core';
import { JsonWebToken } from 'src/app/_models/helpers/jsonWebToken';

@Injectable({
  providedIn: 'root',
})
export class StorageUserService {
  /** Ustawianie nazwy uzytkownika */
  setUserName = (username: string) => sessionStorage.setItem('username', username);

  /** Pobieranie nazwy uzytkownika */
  getUserName = () => sessionStorage.getItem('username');

  /** Ustawianie zalogowanego uzytkownika */
  setLoggedUser = (token: JsonWebToken) => sessionStorage.setItem('currentUser', JSON.stringify(token));

  /** Pobieranie id uzytkownika */
  getLoggedUser = (): JsonWebToken => JSON.parse(sessionStorage.getItem('currentUser'));

  /** Czyszczenie sesji uzytkownika */
  clearSession = () => {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('currentUser');
  };
}
