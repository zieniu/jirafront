import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/_models/user/user';
import { RegisterUser } from 'src/app/_models/user/registerUser';
import { Observable } from 'rxjs';
import { UpdateUser } from 'src/app/_models/user/updateUser';

@Injectable({
  providedIn: 'root',
})
export class UserHttpService {
  constructor(private http: HttpClient) {}

  linkHttp = `${environment.apiUrl}api/user/`;

  /** Get all users */
  getUsers(): Observable<Array<User>> {
    return this.http.get<User[]>(this.linkHttp);
  }

  /** Get user by id */
  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.linkHttp + id);
  }

  /** Register new user */
  registerUser(model: RegisterUser) {
    return this.http.post(this.linkHttp + 'register', model);
  }

  /** Update user */
  updateUser(user: UpdateUser) {
    return this.http.put(this.linkHttp + 'update/' + user.id, user);
  }

  /** Change password */
  updateUserPassword(id: number, password: string) {
    const params = new HttpParams();
    params.set('password', password);

    return this.http.put(this.linkHttp + 'updatePassword/' + id, params);
  }

  /** Delete user */
  deleteUser(id: number) {
    return this.http.delete(this.linkHttp + 'delete/' + id);
  }

  /** Restore user */
  restoreUser(id: number) {
    return this.http.get(this.linkHttp + 'restore/' + id);
  }
}
