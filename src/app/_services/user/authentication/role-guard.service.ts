import { Injectable } from '@angular/core';
import * as decode from 'jwt-decode';
import { StorageUserService } from '../../helpers/storage-user.service';

@Injectable({
  providedIn: 'root',
})
export class RoleGuardService {
  constructor(private storageUserService: StorageUserService) {}

  checkPermission(levelAccess: number) {
    const currentUser = this.storageUserService.getLoggedUser().accessLevel;
    if (currentUser < levelAccess) {
      return false;
    }
    return true;
  }
}
