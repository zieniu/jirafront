import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/_models/user/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private choosenUser = new BehaviorSubject<User>(null); // aktualnie wybrany uzytkownik

  /** Aktualnie wybrany uzytkownik */
  choosenUser$ = this.choosenUser.asObservable();

  /** Ustawianie uzytkownika */
  setChoosenUser(user: User) {
    this.choosenUser.next(user);
  }
}
