import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Notification } from 'src/app/_models/notification/notification';

@Injectable({ providedIn: 'root' })
export class NotificationService {
  /** **************************** */
  private currentSelectedNotification = new BehaviorSubject<Notification>(null);
  /** Aktualnie wybrane zgłoszenie */
  currentSelectedNotification$ = this.currentSelectedNotification.asObservable();
  /** **************************** */
  /** **************************** */
  private detectNewNotification = new Subject<Notification>();
  /** Wykrywanie nowego zgłoszenia */
  detectNewNotification$ = this.detectNewNotification.asObservable();
  /** ************************* */

  /** Ustawianie aktualnie wybranego powiadomienia */
  setCurrentSelectNotification(notification: Notification) {
    this.currentSelectedNotification.next(notification);
  }

  /** Ustawianie nowego zgloszenia */
  setNewNotification(notification: Notification) {
    this.detectNewNotification.next(notification);
  }
}
