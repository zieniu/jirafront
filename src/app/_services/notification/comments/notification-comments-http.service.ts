import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { NotiComment } from 'src/app/_models/notification/comments/notiComment';
import { NewNotiComment } from 'src/app/_models/notification/comments/newNotiComment';

@Injectable({
  providedIn: 'root',
})
export class NotificationCommentsHttpService {
  constructor(private http: HttpClient) {}

  linkHttp = `${environment.apiUrl}api/comment/`;

  /**
   * Pobieranie wiadomosci z serwera
   * @param page strona wiadomości iterowana po 10
   * @param notificationId id zgloszenia
   */
  getCommentsByNotificationAndPage(page: number, notificationId: number): Observable<Array<NotiComment>> {
    const params = new HttpParams().set('page', page.toString()).set('notificationId', notificationId.toString());
    return this.http.get<Array<NotiComment>>(this.linkHttp, { params });
  }

  /** Dodawanie nowego komentarza */
  addNewComment(comment: NewNotiComment): Observable<NotiComment> {
    return this.http.post<NotiComment>(this.linkHttp, comment);
  }

  /** Usuwanie komentarza */
  deleteComment(id: number) {
    return this.http.delete(this.linkHttp + id);
  }
}
