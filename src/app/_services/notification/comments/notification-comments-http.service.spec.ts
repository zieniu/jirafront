import { TestBed } from '@angular/core/testing';

import { NotificationCommentsHttpService } from './notification-comments-http.service';

describe('NotificationCommentsHttpService', () => {
  let service: NotificationCommentsHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NotificationCommentsHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
