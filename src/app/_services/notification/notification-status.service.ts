import { Injectable } from '@angular/core';
import { StatusNotification } from 'src/app/_models/notification/helpers/statusNotification';

@Injectable()
export class NotificationStatusService {
  /** Zwracanie klasy dotyczącej statusu zgłoszenia */
  getClassStatusNotification(status: number): any {
    if (status === StatusNotification.Oczekuje_na_wsparcie) {
      return { 'status-waiting-for-support': true };
    } else if (status === StatusNotification.Oczekuje_na_klienta) {
      return { 'status-waiting-for-customer': true };
    } else if (status === StatusNotification.Oczekuje_na_wsparcie_zespołu_zewnetrznego) {
      return { 'status-waiting-for-external-support': true };
    } else if (status === StatusNotification.Rozwiazane) {
      return { 'status-completed': true };
    } else if (status === StatusNotification.W_trakcie) {
      return { 'status-in-progress': true };
    } else if (status === StatusNotification.Przypisane_do_użytkownika) {
      return { 'status-assigned': true };
    }
  }
}
