import { Component, OnInit } from '@angular/core';
import { TypeNotification } from '../notifications-list-by-type';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';

@Component({
  templateUrl: './notifications-tabs.component.html',
  styleUrls: ['./notifications-tabs.component.scss'],
})
export class NotificationsTabsComponent implements OnInit {
typeNotification = TypeNotification; // typ zgłoszeń
userPermission = UserPermission;

  ngOnInit(): void {}

  constructor(public roleGuardService:RoleGuardService) {  }
}
