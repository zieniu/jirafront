import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { SharedModule } from 'src/app/_shared/shared.module';
import { NotificationsTabsComponent } from '.';
import { NotificationsListByTypeModule } from '../notifications-list-by-type/notifications-list-by-type.module';
import { NotificationsTabsRoutingModule } from './notifications-tabs-routing.module';

@NgModule({
  declarations: [NotificationsTabsComponent],
  imports: [CommonModule, SharedModule, AngularMaterialModule,
    NotificationsTabsRoutingModule,
    NotificationsListByTypeModule],
})
export class NotificationsTabsModule {}
