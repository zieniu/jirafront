import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationChangeStatusDialogComponent } from './notification-change-status-dialog.component';

describe('NotificationChangeStatusDialogComponent', () => {
  let component: NotificationChangeStatusDialogComponent;
  let fixture: ComponentFixture<NotificationChangeStatusDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationChangeStatusDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationChangeStatusDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
