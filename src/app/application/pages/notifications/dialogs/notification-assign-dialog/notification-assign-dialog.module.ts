import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { DragAndDropModule } from 'src/app/application/components/drag-and-drop/drag-and-drop.module';
import { NotificationAssignDialogComponent } from './notification-assign-dialog.component';



@NgModule({
  declarations: [
    NotificationAssignDialogComponent
  ],
  imports: [
    CommonModule,
    DragAndDropModule,
    ReactiveFormsModule,
    AngularMaterialModule
  ],
  entryComponents: [
    NotificationAssignDialogComponent
  ]
})
export class NotificationAssignDialogModule { }
