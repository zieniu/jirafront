import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationAssignDialogComponent } from './notification-assign-dialog.component';

describe('NotificationAssignDialogComponent', () => {
  let component: NotificationAssignDialogComponent;
  let fixture: ComponentFixture<NotificationAssignDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationAssignDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationAssignDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
