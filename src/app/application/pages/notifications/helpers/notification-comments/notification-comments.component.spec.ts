import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationCommentsComponent } from './notification-comments.component';

describe('NotificationCommentsComponent', () => {
  let component: NotificationCommentsComponent;
  let fixture: ComponentFixture<NotificationCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
