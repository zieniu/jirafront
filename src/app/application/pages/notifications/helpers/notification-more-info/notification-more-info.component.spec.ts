import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationMoreInfoComponent } from './notification-more-info.component';

describe('NotificationMoreInfoComponent', () => {
  let component: NotificationMoreInfoComponent;
  let fixture: ComponentFixture<NotificationMoreInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationMoreInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationMoreInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
