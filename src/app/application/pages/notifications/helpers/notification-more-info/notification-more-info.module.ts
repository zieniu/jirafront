import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationMoreInfoComponent } from './notification-more-info.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  declarations: [NotificationMoreInfoComponent],
  imports: [CommonModule, ReactiveFormsModule, AngularEditorModule, AngularMaterialModule],
  exports: [NotificationMoreInfoComponent],
})
export class NotificationMoreInfoModule {}
