import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationMoreInfoDetailComponent } from './notification-more-info-detail.component';


const routes: Routes = [
  { path: '', component: NotificationMoreInfoDetailComponent,data: { animation: 'notMoreInfo' } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NofiticationMoreInfoDetailRoutingModule { }
