import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationMoreInfoModule } from '../helpers/notification-more-info/notification-more-info.module';
import { DragAndDropModule } from 'src/app/application/components/drag-and-drop/drag-and-drop.module';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { NotificationStatusService } from 'src/app/_services/notification/notification-status.service';
import { SharedModule } from 'src/app/_shared/shared.module';
import { NofiticationMoreInfoDetailRoutingModule } from './notification-more-info-detail-routing.module';
import { FormsModule } from '@angular/forms';

import { NotificationMoreInfoDetailComponent } from '.';
import { NotificationCommentsModule } from '../helpers/notification-comments/notification-comments.module';


@NgModule({
  declarations: [
    NotificationMoreInfoDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    DragAndDropModule,
    AngularMaterialModule,
    NotificationMoreInfoModule,
    NotificationCommentsModule,
    NofiticationMoreInfoDetailRoutingModule,
  ],
  providers: [
    NotificationStatusService
  ]
})
export class NotificationMoreInfoDetailModule { }
