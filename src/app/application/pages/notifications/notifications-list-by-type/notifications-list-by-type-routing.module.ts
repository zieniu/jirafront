import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificationsListByTypeComponent } from '.';

const routes: Routes = [
  {
    path: '',
    component: NotificationsListByTypeComponent,
    data: { animation: 'notListType' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationsListByTypeRoutingModule {}
