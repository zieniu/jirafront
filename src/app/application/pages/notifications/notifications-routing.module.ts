import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotificationsComponent } from './notifications.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationsComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./notifications-tabs/notifications-tabs.module').then((m) => m.NotificationsTabsModule),
      },
      {
        path: 'assigned',
        loadChildren: () =>
          import('./notifications-list-by-type/notifications-list-by-type.module').then(
            (m) => m.NotificationsListByTypeModule
          ),
      },
      {
        path: 'published',
        loadChildren: () =>
          import('./notifications-list-by-type/notifications-list-by-type.module').then(
            (m) => m.NotificationsListByTypeModule
          ),
      },
      {
        path: 'detail/:id',
        loadChildren: () =>
          import('./notification-more-info-detail/notification-more-info-detail.module').then(
            (m) => m.NotificationMoreInfoDetailModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotificationsRoutingModule {}
