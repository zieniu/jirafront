import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/_shared/shared.module';
import { EnumPermissionToStrPipe } from 'src/app/_shared/pipe/enum-permission-to-str.pipe';
import { UserStatusPipe } from 'src/app/_shared/pipe/user/user-status.pipe';


@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AngularMaterialModule,
    UserProfileRoutingModule
  ]
})
export class UserProfileModule { }
