import { Component, OnInit, OnDestroy } from '@angular/core';
import { TooltipOpt } from 'src/app/_models/helpers/tooltip';
import { NotificationHttpService } from 'src/app/_services/notification/notification-http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';
import { StatusNotification } from 'src/app/_models/notification/helpers/statusNotification';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/_services/user/user.service';
import { Subscription } from 'rxjs';
import { first, debounceTime } from 'rxjs/operators';
import { User } from 'src/app/_models/user/user';
import { UserHttpService } from 'src/app/_services/user/user-http.service';
import { Location, DatePipe } from '@angular/common';
import { UserChangePermissionDialogComponent } from '../user-change-permission-dialog/user-change-permission-dialog.component';
import { ConfirmDialogComponent } from 'src/app/application/components/confirm-dialog';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';
import { MatDialog } from '@angular/material/dialog';
import { EnumPermissionToStrPipe } from 'src/app/_shared/pipe/enum-permission-to-str.pipe';
import { UserStatusPipe } from 'src/app/_shared/pipe/user/user-status.pipe';
import { UpdateUser } from 'src/app/_models/user/updateUser';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit, OnDestroy, TooltipOpt {
  matTooltipShowDelay = 500;
  matTooltipHideDelay = 500;

  statInProgress = 0;
  statWaitingForExternalSupport = 0;
  statWaitingForCustomer = 0;
  statCompleted = 0;
  statAssigned = 0;

  userId: number; // id przegladanego uzytkownika
  editMode = false; // tryb edycji
  isEditMode = false; // wykrywanie zmian w widoku
  owner = false; // okreslanie kto otwiera profil wlasciciel czy nie

  user: User; // aktualnie przegladany uzytkownik
  userPermission = UserPermission;

  userForm = this.formBuilder.group({
    email: [{ value: '', disabled: true }, Validators.required],
    name: [{ value: '', disabled: true }, Validators.required],
    accessLevel: ['', Validators.required],
    dateCreated: ['', Validators.required],
    deleted: ['', Validators.required],
  });

  /** ************************ SUBSCRIPTION ********************** */
  getUserSub: Subscription;
  userEmailSub: Subscription;
  userNameSub: Subscription;
  /** ************************************************************ */

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackBarService: SnackBarService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private location: Location,
    private userHttpService: UserHttpService,
    private dialog: MatDialog,
    private router: Router,
    private enumPermissionToStrPipe: EnumPermissionToStrPipe,
    private datePipe: DatePipe,
    private userStatusPipe: UserStatusPipe,
    private storageUserService: StorageUserService,
    private notificationHttpService: NotificationHttpService,
    public roleGuardService: RoleGuardService
  ) {
    this.detectParam();
  }

  ngOnInit(): void {
    this.getStatistics();
    this.getUser();
    this.detectChanges();
    this.detectEditMode();
  }

  ngOnDestroy(): void {
    if (this.getUserSub !== undefined) this.getUserSub.unsubscribe();
    if (this.userEmailSub !== undefined) this.userEmailSub.unsubscribe();
    if (this.userNameSub !== undefined) this.userNameSub.unsubscribe();
  }

  /** Powrót do poprzedniej lokalizacji */
  goBack() {
    this.location.back();
  }

  /**
   * Zmiana uprawnien użytkownika
   */
  changeUserPermission(choosenUser: User) {
    const user: User = new User();

    if (choosenUser !== undefined) {
      Object.assign(user, choosenUser);
    }

    const accessLevel = user.accessLevel;
    const dialogRef = this.dialog.open(UserChangePermissionDialogComponent, {
      data: { accessLevel },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        user.accessLevel = parseInt(result, 10); // podmiana praw dostępu
        const updatedUser = new UpdateUser();
        updatedUser.updateUser(user);
        this.userHttpService.updateUser(updatedUser).subscribe(
          () => {
            this.setUserAccessLevel(result);
            this.snackBarService.openSnackBar(
              `Udana zmiana uprawnien użytkownika ${user.name}`,
              '',
              SnackBarStatusAction.Success
            );
          },
          () => {
            this.snackBarService.openSnackBar(
              `Nieudana zmiana uprawnien użytkownika ${user.name}`,
              '',
              SnackBarStatusAction.Error
            );
          }
        );
      }
    });
  }

  openUpdateConfirmDialog() {
    const header = 'Potwierdzenie';
    const content = 'Czy aby napewno chcesz zaktualizwać użytkownika?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        const updatedUser = new UpdateUser();
        updatedUser.updateUser(this.user);
        this.userHttpService.updateUser(updatedUser).subscribe(
          () => {
            this.isEditMode = false;
            this.snackBarService.openSnackBar('Udana aktualizacja użytkownika', '', SnackBarStatusAction.Success);
          },
          () => {
            this.snackBarService.openSnackBar('Nieudana aktualizacja użytkownika', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }

  /**
   * Przywracanie użytkownika
   */
  userRestore(choosenUser: User) {
    const header = 'Przywracanie użytkownika';
    const content = 'Czy aby napewno chcesz przywrócic użytkownika?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userHttpService.restoreUser(choosenUser.id).subscribe(
          () => {
            this.setUserAvailability(0);
            this.snackBarService.openSnackBar(
              `Udane przywrócenie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Success
            );
          },
          () => {
            this.snackBarService.openSnackBar(
              `Nieudane przywrócenie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Error
            );
          }
        );
      }
    });
  }

  /**
   * Usuwanie użytkownika
   */
  userDelete(choosenUser: User) {
    const header = 'Usuwanie użytkownika';
    const content = 'Czy aby napewno chcesz usunąć użytkownika?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userHttpService.deleteUser(choosenUser.id).subscribe(
          () => {
            this.setUserAvailability(1);
            this.snackBarService.openSnackBar(
              `Udane usunięcie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Success
            );
          },
          () => {
            this.snackBarService.openSnackBar(
              `Nieudane usunięcie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Error
            );
          }
        );
      }
    });
  }

  /** Przejscie do nowej karty */
  openNotificationLink(address: string) {
    const userId = this.userId;
    this.router.navigate([`/pages/notifications/${address}`], {
      queryParams: { userId },
      relativeTo: this.activatedRoute,
    });
  }

  /** Pobieranie danych o uzytkowniku */
  private getUser() {
    const user = this.storageUserService.getLoggedUser();

    if (user.userId === this.userId) {
      this.getUserSub = this.userHttpService.getUserById(user.userId).subscribe((next) => {
        if (next !== null) {
          this.user = next;
          this.setForm();
        }
      });
    } else {
      this.getUserSub = this.userService.choosenUser$.pipe(first()).subscribe((next) => {
        if (next !== null) {
          this.user = next;
          this.setForm();
        }
      });
    }
  }

  /** Pobieranie statystyk */
  private getStatistics() {
    this.notificationHttpService.getNotificationsByAssignedUserId(this.userId).subscribe(
      (next) => {
        for (const status in StatusNotification) {
          if (!isNaN(Number(status))) {
            switch (parseInt(status, 10)) {
              case StatusNotification.Oczekuje_na_klienta:
                this.statWaitingForCustomer = next.filter(
                  (s) => s.status === StatusNotification.Oczekuje_na_klienta
                ).length;
                break;
              case StatusNotification.Oczekuje_na_wsparcie_zespołu_zewnetrznego:
                this.statWaitingForExternalSupport = next.filter(
                  (s) => s.status === StatusNotification.Oczekuje_na_wsparcie_zespołu_zewnetrznego
                ).length;
                break;
              case StatusNotification.Rozwiazane:
                this.statCompleted = next.filter((s) => s.status === StatusNotification.Rozwiazane).length;
                break;
              case StatusNotification.W_trakcie:
                this.statInProgress = next.filter((s) => s.status === StatusNotification.W_trakcie).length;
                break;
              case StatusNotification.Przypisane_do_użytkownika:
                this.statAssigned = next.filter((s) => s.status === StatusNotification.Przypisane_do_użytkownika).length;
                break;
            }
          }
        }
      },
      () => {
        this.snackBarService.openSnackBar('Nieudane pobranie statystyk użytkownika', '', SnackBarStatusAction.Error);
      }
    );
  }

  /** Inicjalizacja formatki */
  private setForm() {
    this.userForm.controls.email.setValue(this.user?.email);
    this.userForm.controls.name.setValue(this.user?.name);
    this.userForm.controls.accessLevel.setValue(this.enumPermissionToStrPipe.transform(this.user?.accessLevel));
    this.userForm.controls.dateCreated.setValue(this.datePipe.transform(this.user?.dateCreated, 'dd/MM/yyyy'));
    this.userForm.controls.deleted.setValue(this.userStatusPipe.transform(this.user?.deleted));
  }

  /** Wykrywanie z jakim id uzytkownika z url badz serwisu */
  private detectParam() {
    const editMode = this.activatedRoute.snapshot.queryParamMap.get('edit');

    this.editMode = editMode === 'true'; // przypisywanie trybu edycji

    const owner = this.activatedRoute.snapshot.queryParamMap.get('owner');
    this.owner = owner === 'true';

    this.userId = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10); // przypisywanie id uzytkownika
  }

  /** Ustawianie poziomu dostepu uzytkownika */
  private setUserAccessLevel(level: number) {
    this.user.accessLevel = level;
    this.userForm.controls.accessLevel.setValue(this.enumPermissionToStrPipe.transform(level));
  }

  /** Ustawianie dostepnosci uzytkownika */
  private setUserAvailability(availability: number) {
    this.user.deleted = availability;
    this.userForm.controls.deleted.setValue(this.userStatusPipe.transform(availability));
  }

  /** Okreslanie czy mamy doczynienia z trybem edycji */
  private detectEditMode() {
    if (this.editMode) {
      this.userForm.controls.email.enable();
      this.userForm.controls.name.enable();
    }
  }

  /** Wykrywanie zmian w widoku */
  private detectChanges() {
    this.userEmailSub = this.userForm
      .get('email')
      .valueChanges.pipe(debounceTime(500))
      .subscribe((src) => {
        if (src !== this.user.email) {
          this.user.email = src;
          this.isEditMode = true;
        }
      });
    this.userNameSub = this.userForm
      .get('name')
      .valueChanges.pipe(debounceTime(500))
      .subscribe((src) => {
        if (src !== this.user.name) {
          this.user.name = src;
          this.isEditMode = true;
        }
      });
  }
}
