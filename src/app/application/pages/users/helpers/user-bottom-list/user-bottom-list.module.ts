import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserBottomListComponent } from './user-bottom-list.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';



@NgModule({
  declarations: [
    UserBottomListComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ]
})
export class UserBottomListModule { }
