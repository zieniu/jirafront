import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
import { RoleGuard } from 'src/app/_guards/role.guard';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    canActivate: [RoleGuard],
    data: { expectedRole: UserPermission.ServiceDesk, animation: 'users' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule {}
