import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserChangePermissionDialogComponent } from './user-change-permission-dialog.component';

describe('UserChangePermissionDialogComponent', () => {
  let component: UserChangePermissionDialogComponent;
  let fixture: ComponentFixture<UserChangePermissionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserChangePermissionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserChangePermissionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
