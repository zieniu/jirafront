import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { SharedModule } from 'src/app/_shared/shared.module';
import { UserChangePermissionDialogComponent } from './user-change-permission-dialog/user-change-permission-dialog.component';
import { DragAndDropModule } from '../../components/drag-and-drop/drag-and-drop.module';
import { ConfirmDialogModule } from '../../components/confirm-dialog/confirm-dialog.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UsersComponent,
    UserChangePermissionDialogComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    DragAndDropModule,
    UsersRoutingModule,
    AngularMaterialModule,
  ],
  entryComponents:[
    UserChangePermissionDialogComponent
  ]
})
export class UsersModule { }
