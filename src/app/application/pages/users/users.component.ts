import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { User } from 'src/app/_models/user/user';
import { MatDialog } from '@angular/material/dialog';
import { UserChangePermissionDialogComponent } from './user-change-permission-dialog/user-change-permission-dialog.component';
import { ConfirmDialogComponent } from '../../components/confirm-dialog';
import { UserHttpService } from 'src/app/_services/user/user-http.service';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';
import { Route } from '@angular/compiler/src/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StorageUserService } from 'src/app/_services/helpers/storage-user.service';
import { RoleGuardService } from 'src/app/_services/user/authentication/role-guard.service';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';
import { UserService } from 'src/app/_services/user/user.service';
import { UpdateUser } from 'src/app/_models/user/updateUser';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  matTooltipShowDelay = 500; // opoznienie w wyswietlenie podpowiedzi
  matTooltipHideDelay = 300; // opoznienie w ukryciu podpowiedzi

  displayedColumns = ['id', 'name', 'email', 'accessLevel', 'buttons']; // kolumny
  dataSource: MatTableDataSource<User>; // dane tabularyczne
  users: Array<User>; // spis uzytkownikow
  deletedUserColor: '#f1020225'; // kolor usunietych uzytkownikow

  userId: number; // aktualnie zalogowany uzytkownik
  userPermission = UserPermission; // enum ról

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(
    private dialog: MatDialog,
    private userHttpService: UserHttpService,
    private snackBarService: SnackBarService,
    private router: Router,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    public roleGuardService: RoleGuardService,
    private storageUserService: StorageUserService
  ) {
    this.userId = this.storageUserService.getLoggedUser().userId;
  }

  ngOnInit() {
    this.getUsers();
  }

  /**
   * Filtrowanie tabeli
   */
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /**
   * Zmiana uprawnien użytkownika
   */
  changeUserPermission(choosenUser: User) {
    const user: User = new User();

    if (choosenUser !== undefined) {
      Object.assign(user, choosenUser);
    }

    const accessLevel = user.accessLevel;
    const dialogRef = this.dialog.open(UserChangePermissionDialogComponent, {
      data: { accessLevel },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        user.accessLevel = parseInt(result, 10); // podmiana praw dostępu
        const updatedUser = new UpdateUser();
        updatedUser.updateUser(user);
        this.userHttpService.updateUser(updatedUser).subscribe(
          (src) => {
            choosenUser.accessLevel = result;
            this.table.renderRows();
            this.snackBarService.openSnackBar(
              `Udana zmiana uprawnien użytkownika ${user.name}`,
              '',
              SnackBarStatusAction.Success
            );
          },
          (error) => {
            this.snackBarService.openSnackBar(
              `Nieudana zmiana uprawnien użytkownika ${user.name}`,
              '',
              SnackBarStatusAction.Error
            );
          }
        );
      }
    });
  }

  /**
   * Przywracanie użytkownika
   */
  userRestore(choosenUser: User) {
    const header = 'Przywracanie użytkownika';
    const content = 'Czy aby napewno chcesz przywrócic użytkownika?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userHttpService.restoreUser(choosenUser.id).subscribe(
          (src) => {
            choosenUser.deleted = 0;
            this.table.renderRows();
            this.snackBarService.openSnackBar(
              `Udane przywrócenie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Success
            );
          },
          (error) => {
            this.snackBarService.openSnackBar(
              `Nieudane przywrócenie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Error
            );
          }
        );
      }
    });
  }

  /**
   * Usuwanie użytkownika
   */
  userDelete(choosenUser: User) {
    const header = 'Usuwanie użytkownika';
    const content = 'Czy aby napewno chcesz usunąć użytkownika?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { header, content },
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userHttpService.deleteUser(choosenUser.id).subscribe(
          (src) => {
            choosenUser.deleted = 1;
            this.table.renderRows();
            this.snackBarService.openSnackBar(
              `Udane usunięcie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Success
            );
          },
          (error) => {
            this.snackBarService.openSnackBar(
              `Nieudane usunięcie użytkownika ${choosenUser.name}`,
              '',
              SnackBarStatusAction.Error
            );
          }
        );
      }
    });
  }

  /** Przejscie do szczegółow użytkownika */
  openUserLink(row: User) {
    this.userService.setChoosenUser(row);
    if (this.roleGuardService.checkPermission(UserPermission.Admin)) {
      // jezeli admin
      const edit = true;
      this.router.navigate([`../../user/${row.id}`], {
        queryParams: { edit },
        relativeTo: this.activatedRoute,
      });
    } else {
      this.router.navigate([`../../user/${row.id}`], {
        relativeTo: this.activatedRoute,
      });
    }
  }

  /** Przejscie do nowej karty */
  openNotificationLink(address: string) {
    const userId = this.storageUserService.getLoggedUser().userId;

    this.router.navigate([`/pages/notifications/${address}`], {
      queryParams: { userId },
      relativeTo: this.activatedRoute,
    });
  }

  private getUsers() {
    this.userHttpService.getUsers().subscribe((next) => {
      this.users = next;
    }, (error) => {
      this.snackBarService.openSnackBar('Nieudane pobranie użytkowników', '', SnackBarStatusAction.Error);
    }, () => {
      this.dataSource = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
}
