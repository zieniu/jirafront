import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from 'src/app/_services/user/authentication/authentication.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { UserBottomListComponent } from '../../pages/users/helpers/user-bottom-list/user-bottom-list.component';
import { MatDialog } from '@angular/material/dialog';
import { NotificationCreateNewDialogComponent } from '../../pages/notifications/dialogs/notification-create-new-dialog';
import { NewNotification } from 'src/app/_models/notification/newNotification';
import { NotificationService } from 'src/app/_services/notification/notification.service';
import { NotificationHttpService } from 'src/app/_services/notification/notification-http.service';
import { SnackBarService, SnackBarStatusAction } from 'src/app/_services/helpers/snack-bar.service';
import { Notification } from 'src/app/_models/notification/notification';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  matTooltipShowDelay = 300; // opoznienie w wyswietlenie podpowiedzi
  matTooltipHideDelay = 300; // opoznienie w ukryciu podpowiedzi
  @Output() eventMenu = new EventEmitter<boolean>();

  constructor(
    private authenticationService: AuthenticationService,
    private _bottomSheet: MatBottomSheet,
    private dialog: MatDialog,
    private snackBarService: SnackBarService,
    private notificationService: NotificationService,
    private notificationHttpService: NotificationHttpService
  ) {}

  ngOnInit() {}

  /** Wylogowywanie z aplikacji */
  logout() {
    this.authenticationService.logout();
  }

  /**
   * Detekcja zmian w polozeniu sidenav
   */
  emitMenuClickEvent() {
    this.eventMenu.emit();
  }

  /**
   * Otwieranie listy z opcjami dla uzytkownika
   */
  openUserBottomSheet(): void {
    this._bottomSheet.open(UserBottomListComponent);
  }

  /**
   * Tworzenie nowego zgloszenia
   */
  createNewNotification() {
    const header = 'Tworzenie zgłoszenia';

    const dialogRef = this.dialog.open(NotificationCreateNewDialogComponent, {
      data: { header },
      hasBackdrop: false,
      width: '100%',
      maxWidth: '700px',
    });

    dialogRef.afterClosed().subscribe((result: NewNotification) => {
      if (result) {
        this.notificationHttpService.createNotification(result).subscribe(
          (next) => {
            this.snackBarService.openSnackBar('Udane dodanie nowego zgłoszenia', '', SnackBarStatusAction.Success);
          },
          (error) => {
            this.snackBarService.openSnackBar('Nieudane dodanie zgłoszenia', '', SnackBarStatusAction.Error);
          }
        );
      }
    });
  }
}
