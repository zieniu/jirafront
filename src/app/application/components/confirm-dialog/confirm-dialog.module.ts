import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog.component';
import { AngularMaterialModule } from 'src/app/_modules/angular-material.module';
import { DragAndDropModule } from '../drag-and-drop/drag-and-drop.module';



@NgModule({
  declarations: [
    ConfirmDialogComponent
  ],
  imports: [
    CommonModule,
    DragAndDropModule,
    AngularMaterialModule
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class ConfirmDialogModule { }
