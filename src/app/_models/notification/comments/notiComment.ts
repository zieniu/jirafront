export interface NotiComment {
    id?: number;
    description: string;
    userName: string;
    saveTime: Date;
  }