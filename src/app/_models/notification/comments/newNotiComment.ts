export interface NewNotiComment {
  description: string;
  userId: number;
  notificationId: number;
}
