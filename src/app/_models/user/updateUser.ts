import { UserPermission } from './helpers/userPermission';
import { User } from './user';

export class UpdateUser {
    id:number;
    email:string;
    name:string;
    accessLevel:number;

    updateUser(user:User){
        this.id = user.id;
        this.name = user.name;
        this.email = user.email;
        this.accessLevel = user.accessLevel;
    }
}