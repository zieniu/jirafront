/** Uprawnienia usera */
export enum UserPermission {
    Normal = 0,
    ServiceDesk = 1,
    Admin = 2,
  }