import { UserPermission } from './helpers/userPermission';

export class User {
  id?: number;
  email: string;
  name: string;
  accessLevel?: UserPermission;
  deleted?: AvailableUserEnum;
  dateCreated?: Date;

  updateUser(_user: User) {
    this.id = _user.id;
    this.email = _user.email;
    this.name = _user.name;
    this.accessLevel = _user.accessLevel;
    this.deleted = _user.deleted;
    this.dateCreated = _user.dateCreated;
  }
}

/** Typ wyliczeniowy informujący czy edytujemy tworzymy czy tylko do odczytu */
export enum UserStatusEnum {
  Edit = 0,
  Create = 1,
  Readonly = 2,
  Admin = 3,
}

/** Okreslanie czy user jest dostepny */
export enum AvailableUserEnum {
  Available = 0,
  Deleted = 1,
}


