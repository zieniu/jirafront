import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: any) {
    const typeCart = [];
    const obj = Object.keys(data);
    const values = obj.slice(0, obj.length / 2);
    const viewValues = obj.slice(obj.length / 2);

    for (let i = 0; i < obj.length / 2; i++) {
      const reg = /_/g; // zamiana z _ na spacje w kazdym opisie
      const viewValueReg = viewValues[i].replace(reg, ' ');
      typeCart.push({ viewValue: viewValueReg, value: values[i] });
    }
    return typeCart;
  }
}
