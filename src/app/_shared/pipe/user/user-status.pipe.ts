import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userStatus'
})
export class UserStatusPipe implements PipeTransform {

  transform(value: number): string {
    if(value === 0){
      return 'Dostępny';
    } else {
      return 'Zbanowany'
    }
  }
}
