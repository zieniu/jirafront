import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeNotificationPipe',
})
export class TypeNotificationPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value === 0) {
      return 'error';
    } else {
      return 'record_voice_over';
    }
  }
}
