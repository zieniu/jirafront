import { Pipe, PipeTransform } from '@angular/core';
import { UserPermission } from 'src/app/_models/user/helpers/userPermission';

@Pipe({
  name: 'enumPermissionToStr'
})
export class EnumPermissionToStrPipe implements PipeTransform {

  transform(value: number) {
    return UserPermission[value];
  }
}
