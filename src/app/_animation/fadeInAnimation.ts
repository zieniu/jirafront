import { trigger, transition, style, query, animateChild, animate, group } from '@angular/animations';
import { Optional } from '@angular/core';

export const fadeInAnimation =
  trigger('fadeInAnimation', [
    transition('* <=> *', [
      style({ position: 'relative' }),
      query(':enter, :leave', [
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        })
      ],
        { optional: true }),
      query(':enter', [
        style({ opacity: 0 })
      ],
        { optional: true }),
      group([
        query(':enter', [
          animate('500ms ease-out', style({ opacity: 1 }))
        ],
          { optional: true }),
        query(':leave', [
          animate('0ms ease-out', style({ opacity: 0 }))
        ],
          { optional: true })
      ]),
      query(':enter', animateChild(), { optional: true }),
    ])
  ]);
